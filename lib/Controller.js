var _ = require('lodash');
function Controller(){


    var self = this;

    /**
     * Состояние контроллера. Все данные храняться здесь.
     * @type {{}}
     */
    self.state = {
        acl:{
            isAuth: false,
            isAdmin: false,
            idUser: 0
        },
        channels: {

            list: []
        }
    };


    /**
     * Добавить канал в список
     */
    self.addChannel = function(arChannel = {}, bTriggerUpdate = true){

        /*
        -- Проверим на обязательные свойства
         */

        /*
        -- Дополним свойства, кторые не указаны
         */

        var arChannelExamples = {
            id: 23,
            entity_type: 1,
            entity_id: 23,
            author_id: 1234,
            message: 'текст сообщения',
            photos: [],
            videos: [],
            attaches: [],
            like_pos: 0,
            like_neg: 0,
            created_at: ''
        };

        self.state.channels.list.push(arChannel);

        self.triggerUpdate();

    };




    self.triggerUpdate = function(){
        //self.trigger(ControllerFoodSearch.cEVENT_ON_UPDATE, self.getState());
        return self;
    };

    self.onUpdate = function(fFunc){
        self.on(ControllerFoodSearch.cEVENT_ON_UPDATE, fFunc);
        return self;
    };
    self.offUpdate = function(fFunc){
        self.off(ControllerFoodSearch.cEVENT_ON_UPDATE, fFunc);
        return self;
    };



    //Events.observable(self);


}

Controller._instance = null;
Controller.getInstance = function () {
    if(Controller._instance===null){
        Controller._instance = new Controller();
    }
    return Controller._instance;
};


module.exports = Controller;