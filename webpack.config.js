var path = require("path");
var webpack = require('webpack');
module.exports = {
    entry: "./lib/Main.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "mzr-socialchannelscontroller.js",
        publicPath: "dist",
        //sourceMapFilename: "bundle.js.map",
        libraryTarget: "umd",
        library: "mzr-socialchannelscontroller"
    },
    plugins: [
        new webpack.SourceMapDevToolPlugin(
            '[file].map', null,
            "[absolute-resource-path]", "[absolute-resource-path]"),
        //new webpack.optimize.UglifyJsPlugin(),
        new webpack.DefinePlugin({
            'NODE_ENV': JSON.stringify('production')
        })
    ],
    module: {
        loaders: [
            { test: /\.js$/, exclude: /(node_modules|bower_components)/, loader: "babel-loader"}
        ]
    },
    externals: {
        "lodash": "lodash"
    }

};