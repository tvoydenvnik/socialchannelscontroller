(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("lodash"));
	else if(typeof define === 'function' && define.amd)
		define(["lodash"], factory);
	else if(typeof exports === 'object')
		exports["mzr-socialchannelscontroller"] = factory(require("lodash"));
	else
		root["mzr-socialchannelscontroller"] = factory(root["lodash"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "dist";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var Controller = __webpack_require__(1);
	module.exports = {
	
	    Controller: Controller
	};

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(2);
	function Controller() {
	
	    var self = this;
	
	    /**
	     * Состояние контроллера. Все данные храняться здесь.
	     * @type {{}}
	     */
	    self.state = {
	        acl: {
	            isAuth: false,
	            isAdmin: false,
	            idUser: 0
	        },
	        channels: {
	
	            list: []
	        }
	    };
	
	    /**
	     * Добавить канал в
	     */
	    self.addChannel = function () {
	        var arChannel = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
	        var bTriggerUpdate = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];
	
	        /*
	        -- Проверим на обязательные свойства
	         */
	
	        /*
	        -- Дополним свойства, кторые не указаны
	         */
	
	        self.state.channels.list.push(arChannel);
	
	        self.triggerUpdate();
	    };
	
	    self.triggerUpdate = function () {
	        //self.trigger(ControllerFoodSearch.cEVENT_ON_UPDATE, self.getState());
	        return self;
	    };
	
	    self.onUpdate = function (fFunc) {
	        self.on(ControllerFoodSearch.cEVENT_ON_UPDATE, fFunc);
	        return self;
	    };
	    self.offUpdate = function (fFunc) {
	        self.off(ControllerFoodSearch.cEVENT_ON_UPDATE, fFunc);
	        return self;
	    };
	
	    //Events.observable(self);
	}
	
	Controller._instance = null;
	Controller.getInstance = function () {
	    if (Controller._instance === null) {
	        Controller._instance = new Controller();
	    }
	    return Controller._instance;
	};
	
	module.exports = Controller;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }
/******/ ])
});
;
//# sourceMappingURL=mzr-socialchannelscontroller.js.map